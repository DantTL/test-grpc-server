package com.grpccrudserver;

import com.grpccrudserver.proto.HelloRequest;
import com.grpccrudserver.proto.HelloResponse;
import com.grpccrudserver.proto.HelloServiceGrpc;
import io.grpc.stub.StreamObserver;

public class HelloServiceImpl extends HelloServiceGrpc.HelloServiceImplBase {
    @Override
    public void hello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        String firstname = request.getFirstName();
        String lastname = request.getLastName();

        HelloResponse response = HelloResponse.newBuilder()
                .setGreeting("Hola " + firstname + " " + lastname)
                .build();

        responseObserver.onNext(response);

        responseObserver.onCompleted();
    }
}
